package com.example.BlogRestaurante.repositories;

import com.example.BlogRestaurante.entities.City;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface CityRepository extends JpaRepository<City,Integer> {
    City findById(Integer id);
}
