package com.example.BlogRestaurante.repositories;

import com.example.BlogRestaurante.entities.Pais;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface PaisRepository extends JpaRepository<Pais,Integer> {
    Pais findById(Integer id);
}
