package com.example.BlogRestaurante.services;

import com.example.BlogRestaurante.entities.Estado;

public interface EstadoService {
    Iterable <Estado> listAllEstados();
    Estado getEstadoById(Integer id);
    Estado saveEstado(Estado option);
    void deleteEstado(Integer id);
}
