package com.example.BlogRestaurante.services;


import com.example.BlogRestaurante.entities.Pais;

public interface PaisService {
    Iterable<Pais> listAllPaises();
    Pais getPaisById(Integer id);
    Pais savePais(Pais city);
    void deletePais(Integer id);
}
